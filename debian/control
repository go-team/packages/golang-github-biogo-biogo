Source: golang-github-biogo-biogo
Maintainer: Debian Go Packaging Team <team+pkg-go@tracker.debian.org>
Uploaders: Andreas Tille <tille@debian.org>, Nilesh Patra <nilesh@debian.org>
Section: devel
Testsuite: autopkgtest-pkg-go
Priority: optional
Build-Depends: debhelper-compat (= 13),
               dh-golang,
               golang-any,
               golang-github-biogo-hts-dev,
               golang-github-biogo-graph-dev,
               golang-github-biogo-store-dev,
               golang-gopkg-check.v1-dev
Standards-Version: 4.6.0
Vcs-Browser: https://salsa.debian.org/go-team/packages/golang-github-biogo-biogo
Vcs-Git: https://salsa.debian.org/go-team/packages/golang-github-biogo-biogo.git
Homepage: https://github.com/biogo/biogo
Rules-Requires-Root: no
XS-Go-Import-Path: github.com/biogo/biogo

Package: golang-github-biogo-biogo-dev
Architecture: all
Depends: golang-github-biogo-hts-dev,
         golang-gopkg-check.v1-dev,
         ${misc:Depends}
Description: biogo is a bioinformatics library for Go (library)
 Bíogo stems from the need to address the size and structure of modern
 genomic and metagenomic data sets. These properties enforce requirements
 on the libraries and languages used for analysis:
 .
  * speed - size of data sets
  * concurrency - problems often embarrassingly parallelisable
 .
 In addition to the computational burden of massive data set sizes in
 modern genomics there is an increasing need for complex pipelines to
 resolve questions in tightening problem space and also a developing need
 to be able to develop new algorithms to allow novel approaches to
 interesting questions. These issues suggest the need for a simplicity in
 syntax to facilitate:
 .
  * ease of coding
  * checking for correctness in development and particularly in
    peer review
